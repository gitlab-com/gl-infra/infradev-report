"use strict";

const startOfWeek = require("date-fns/start_of_week");
const purgeCache = require("gitlab-issue-report-kit").purgeCache;
const Report = require("gitlab-issue-report-kit").Report;
const IssueCategorizer = require("./issue-categorizer");
const InfradevEpicCategorizer = require("./infradev-epic-categorizer");
const InfradevSummarizer = require("./infradev-summarizer");
const isBefore = require("date-fns/is_before");
const differenceInWeeks = require("date-fns/difference_in_weeks");
const EngineeringAllocator = require("./engineering-allocation");
const config = require("./config.json");

const REPORT_SECTIONS = [
  {
    title: "Priority SLO Overdue :x:",
    description:
      "Issues that violate the SLO for their given priority, as measured from the date the current priority label was first added. See the [issue triage handbook page](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority) for details. Required action: investigate why these issues have not made their SLA and repriotise or escalate.",
    category: "PRIORITY_SLA_VIOLATED",
    dateField: "prioritySLADate"
  },
  {
    title: "Severity SLO Overdue :x:",
    description:
      "Issues that violate the SLO for their given severity, as measured from the date the current severity label was first added. See the [issue triage handbook page](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity) for details. Required action: investigate why these issues have not made their SLA and repriotise or escalate.",
    category: "SEVERITY_SLA_VIOLATED",
    dateField: "severitySLADate"
  },
  {
    title: "Past Due, unassigned :x:",
    description:
      "Issues in milestones that are past due that are unassigned. Required action: investigate why these issues have missed their milestone and repriotise or escalate.",
    category: "PAST_DUE_UNASSIGNED",
    dateField: "milestoneDueDate"
  },
  {
    title: "Past Due :x:",
    description:
      "Issues that are not in review or verification for a milestone that is past due. Required action: investigate why these issues have missed their milestone and repriotise or escalate.",
    category: "PAST_DUE",
    dateField: "milestoneDueDate"
  },
  {
    title: "Stagnant issues :x:",
    description:
      "Issues that are on the backlog or do not have a milestone that are old. Required action: Retriage. Consider dropping the infradev label if the issue is not longer applicable, or escalate.",
    category: "STAGNANT"
  },
  {
    title: "Infradev issues without a milestone :x:",
    description: "Issues that have yet to be assigned any milestone, including the backlog. Required action: assign a milestone.",
    category: "NO_MILESTONE"
  },
  {
    title: "Infradev epics past due date :x:",
    description:
      "Infradev epics that are past their due date. Required action: investigate why these epic have missed their due date and repriotise or escalate.",
    category: "EPIC_PAST_DUE"
  },
  {
    title: "Past Due, in review :neutral_face:",
    description: "Issues that are past due, but currently awaiting verification. Required action: ensure verification process is still underway.",
    category: "PAST_DUE_IN_REVIEW",
    dateField: "milestoneDueDate"
  },
  {
    title: "Past Due, awaiting verification :neutral_face:",
    description: "Issues that are past due, but are in review. Required action: ensure review process is currently underway and escalate is necessary.",
    category: "PAST_DUE_VERIFICATION",
    dateField: "milestoneDueDate"
  },
  {
    title: "Issues due this milestone, but unassigned :neutral_face:",
    description:
      "Issues that have been assigned to the current milestone, but have not been assigned. Required action: confirm that the issue will be delivered in the current milestone, reschedule or escalate.",
    category: "CURRENT_MILESTONE_BUT_UNASSIGNED"
  },
  {
    title: "Recent Issues missing links to incidents :neutral_face:",
    description:
      "Issues marked as infradev less than 2 weeks ago that have not been linked to production incidents or capacity planning issues. Required action: ensure they meet the [requirements for infradev issues](https://about.gitlab.com/handbook/engineering/workflow/#a-guide-to-creating-effective-infradev-issues), add issue relationship links to production issues or capacity planning issues.",
    category: "RECENT_ISSUE_WITHOUT_LINKS"
  },
  {
    title: "Backlog Issues :neutral_face:",
    description: "Issues on the backlog. Required action: occassional retriage.",
    category: "BACKLOG"
  },
  {
    title: "Issues due this milestone :white_check_mark:",
    description: "**FYI** Issues that are currently being worked on. No action required.",
    category: "CURRENT_MILESTONE"
  },
  {
    title: "Recent Infradev Epics :white_check_mark:",
    description:
      "Epics marked as infradev less than 2 weeks ago. Required action: review and ensure they meet the [requirements for infradev issues](https://about.gitlab.com/handbook/engineering/workflow/#a-guide-to-creating-effective-infradev-issues).",
    category: "RECENT_EPIC"
  },
  {
    title: "Recent Infradev Issues :white_check_mark:",
    description:
      "Issues marked as infradev less than 2 weeks ago. Required action: review and ensure they meet the [requirements for infradev issues](https://about.gitlab.com/handbook/engineering/workflow/#a-guide-to-creating-effective-infradev-issues).",
    category: "RECENT_ISSUE"
  },
  {
    title: "Other Infradev Epics :white_check_mark:",
    description: "**FYI** All other infradev epics. No action required. These issues have not been flagged for any reason.",
    category: "OTHER_EPIC"
  },
  {
    title: "Other Infradev Issues :white_check_mark:",
    description: "**FYI** All other infradev issues. No action required. These issues have not been flagged for any reason.",
    category: "OTHER"
  }
];

function getRank (label) {
  if (!label) return 1000;
  const v = label.split(/:+/)[1];
  if (!v) return 1000;
  return parseInt(v, 10) || 1000;
}

function issuePriorityComparator (issue1, issue2) {
  const pi1 = getRank(issue1.priority);
  const pi2 = getRank(issue2.priority);

  if (pi1 === pi2) {
    const si1 = getRank(issue1.severity);
    const si2 = getRank(issue2.severity);
    return si1 - si2;
  }
  return pi1 - pi2;
}

function isOverdueAny (issue) {
  return issue.categories.has("HAS_PRIORITY_SLA") || issue.categories.has("HAS_SEVERITY_SLA")
    ? issue.categories.has("PRIORITY_SLA_VIOLATED") || issue.categories.has("SEVERITY_SLA_VIOLATED")
    : null;
}

function earliestSLA (issue) {
  const prioritySLA = issue.prioritySLADate;
  const severitySLA = issue.severitySLADate;

  if (!prioritySLA) return severitySLA;
  if (!severitySLA) return prioritySLA;

  if (isBefore(prioritySLA, severitySLA)) {
    return prioritySLA;
  } else {
    return severitySLA;
  }
}

class StatusReportGenerator {
  constructor () {
    this.since = this.getStartTime();
  }

  getStartTime () {
    const now = new Date();

    // Weeks roll over at 00h00Z UTC
    return startOfWeek(now, { weekStartsOn: 1 });
  }

  async getStatusReport () {
    const infradevIssueCategories = await new IssueCategorizer("infradev").categorizeIssues();
    const errorBudgetIssueCategories = await new IssueCategorizer("Error Budget Improvement").categorizeIssues();
    const epicCategories = await new InfradevEpicCategorizer().categorizeInfradevEpics();

    let bodyLines = [
      "# Infrastructure Technical Debt / Infradev Status Report",
      "## Group Summary",
      "",
      "This report is intended to be consumed by Infrastructure Engineers and contains detailed information.",
      "If you are looking for a general error-budget report, please visit <https://gitlab.com/gitlab-org/error-budget-reports/-/issues>.",
      "",
      "**Note**: this report uses a rolling 28d window until 00h00Z today for traffic share, ",
      "error budget, apdex component and error component columns."
    ];

    const engineeringAllocator = new EngineeringAllocator();
    const engineeringReport = await engineeringAllocator.report(infradevIssueCategories.get("ALL"), errorBudgetIssueCategories.get("ALL"));
    bodyLines = bodyLines.concat(engineeringReport);

    bodyLines.push("\n-------------------------");

    REPORT_SECTIONS.forEach((section) => {
      const issues = infradevIssueCategories.get(section.category) || [];
      const epics = epicCategories.get(section.category) || [];
      const all = issues.concat(epics);

      if (all.length) {
        bodyLines.push(`\n## ${section.title}\n`);
        if (section.description) {
          bodyLines.push(`${section.description}\n`);
        }
        bodyLines = bodyLines.concat(this.renderIssueList(section, all));
      }
    });

    bodyLines.push("\n-------------------------");

    // Priority Summary
    const issueSummaryPriority = new InfradevSummarizer(
      "Priority",
      infradevIssueCategories.get("ALL"),
      (issue) => (issue.priority ? `~"${issue.priority}"` : "None"),
      "% Overdue on Priority",
      (issue) => (issue.categories.has("HAS_PRIORITY_SLA") ? issue.categories.has("PRIORITY_SLA_VIOLATED") : null),
      (issue) => issue.prioritySLADate
    );
    bodyLines = bodyLines.concat(issueSummaryPriority.render());

    // Severity Summary
    const issueSummarySeverity = new InfradevSummarizer(
      "Severity",
      infradevIssueCategories.get("ALL"),
      (issue) => (issue.severity ? `~"${issue.severity}"` : "None"),
      "% Overdue on Severity",
      (issue) => (issue.categories.has("HAS_SEVERITY_SLA") ? issue.categories.has("SEVERITY_SLA_VIOLATED") : null),
      (issue) => issue.severitySLADate
    );
    bodyLines = bodyLines.concat(issueSummarySeverity.render());

    // Section Summary
    const issueSummarySection = new InfradevSummarizer(
      "Section",
      infradevIssueCategories.get("ALL"),
      (issue) => this.findForPrefix(issue, "section::") || "None",
      "% Overdue",
      isOverdueAny,
      earliestSLA
    );
    bodyLines = bodyLines.concat(issueSummarySection.render());

    bodyLines.push("\n-------------------------");
    bodyLines.push("\nThis status report was [autogenerated](https://gitlab.com/gitlab-com/gl-infra/infradev-report).");
    bodyLines.push("\n/confidential");

    const since = this.since;
    return new Report(
      since, config.project, config.label, true, config.titlePrefix, bodyLines.join("\n")
    );
  }

  findForPrefix (issue, prefix) {
    const i = issue.labels.findIndex((item) => item.startsWith(prefix));
    if (i >= 0) {
      return `~"${issue.labels[i]}"`;
    }
  }

  pickLabels (issue, prefixes) {
    const result = [];
    for (const prefix of prefixes) {
      const i = this.findForPrefix(issue, prefix);
      if (i) {
        result.push(i);
      }
    }
    return result;
  }

  // Renders a list of issues, broken down by `section::` label
  renderIssueList (reportSection, issueList) {
    // Group the list into sections
    const groupedBySection = issueList.reduce((map, issue) => {
      const section = this.findForPrefix(issue, "section::") || "None";
      if (map.has(section)) {
        map.get(section).push(issue);
      } else {
        map.set(section, [issue]);
      }
      return map;
    }, new Map());

    const sections = Array.from(groupedBySection.keys());
    sections.sort(); // Alphabetical

    return sections.flatMap((section) => {
      const sectionIssues = groupedBySection.get(section);

      sectionIssues.sort(issuePriorityComparator); // Descending priority
      const issuesList = sectionIssues.map((issue) => {
        const relatedIncidentCount = issue.relatedLinks.incidents.length;
        const relatedCapacityPlanningCount = issue.relatedLinks.capacityPlanning.length;

        const relations = [];
        if (relatedIncidentCount) {
          relations.push(`${relatedIncidentCount} incident${relatedIncidentCount === 1 ? "" : "s"}`);
        }

        if (relatedCapacityPlanningCount) {
          relations.push(`${relatedCapacityPlanningCount} capacity issue${relatedCapacityPlanningCount === 1 ? "" : "s"}`);
        }

        const relationMarkdown = relations.length ? `<small>(_related_: ${relations.join(", ")})</small>` : "";

        const dateMarkdown = this.formatIssueDate(issue, reportSection.dateField);

        const labels = this.pickLabels(issue, ["priority::", "severity::", "devops::", "group::", "Category:"]);
        return `    1. [${issue.title}](${issue.web_url}) ${relationMarkdown} ${labels.join(" ")} ${dateMarkdown}`;
      });

      return [`1. **Section: ${section}**`].concat(issuesList);
    });
  }

  formatIssueDate (issue, dateField) {
    if (!dateField) return "";
    const date = issue[dateField];
    if (!date) return "";
    const diff = Math.round(differenceInWeeks(date, Date.now()));
    const wrap = (s) => `<small>${s}</small>`;
    if (diff === 0) {
      return wrap("due this week");
    }
    if (diff === -1) {
      return wrap("{- overdue by 1 week  -}");
    }
    if (diff === 1) {
      return wrap("due in 1 week");
    }
    if (diff < 0) {
      return wrap(`{- overdue by ${-diff} weeks -}`);
    }
    return wrap(`due in ${diff} weeks`);
  }
}

async function statusReport () {
  try {
    const statusReportGenerator = new StatusReportGenerator();

    return await statusReportGenerator.getStatusReport();
  } finally {
    try {
      await purgeCache();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error("Cache purge failed: " + e);
    }
  }
}

module.exports = statusReport;
