"use strict";

const fetch = require("gitlab-issue-report-kit").fetch;
const yaml = require("js-yaml");

async function fetchStageGroups () {
  const res = await fetch("https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml");

  const text = await res.text();
  const stages = yaml.load(text).stages;

  return Object.keys(stages).flatMap((stageId) => {
    const v = stages[stageId];
    v.id = stageId;
    const section = v.section;

    return Object.keys(v.groups).map((key) => {
      const w = v.groups[key];
      w.id = key;
      w.stage = stageId;
      w.section = section;
      return w;
    });
  });
}

module.exports = fetchStageGroups;
