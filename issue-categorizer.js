"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;
const setHours = require("date-fns/set_hours");
const setMinutes = require("date-fns/set_minutes");
const setSeconds = require("date-fns/set_seconds");
const setMilliseconds = require("date-fns/set_milliseconds");
const isBefore = require("date-fns/is_before");
const differenceInDays = require("date-fns/difference_in_days");
const addDays = require("date-fns/add_days");
const parse = require("date-fns/parse");

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority
const PRIORITY_SLA_DAYS = {
  "priority::1": 30,
  "priority::2": 90
};

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
const SEVERITY_SLA_DAYS = {
  "severity::1": 30,
  "severity::2": 60,
  "severity::3": 90
};

function parseLocalDate (date) {
  let d = parse(date);
  d = setHours(d, 0);
  d = setMinutes(d, 0);
  d = setSeconds(d, 0);
  d = setMilliseconds(d, 0);
  return d;
}

function isPastDue (date) {
  const d = parseLocalDate(date);
  return isBefore(d, Date.now());
}

class IssueCategorizer {
  constructor (labelName) {
    this.labelName = labelName;
  }

  async getLabelEvents (issue) {
    const firstEvents = new Map();
    for await (const event of urlStreamer(`https://gitlab.com/api/v4/projects/${issue.project_id}/issues/${issue.iid}/resource_label_events`)) {
      if (event.action !== "add" || !event.label) {
        continue;
      }

      const createdAt = parse(event.created_at);
      const label = event.label.name;
      if (!firstEvents.has(label)) {
        firstEvents.set(label, createdAt);
      }
    }

    return firstEvents;
  }

  async categorizeRelatedIssues (issue) {
    const incidentRelations = [];
    const capacityPlanningRelations = [];

    for await (const link of urlStreamer(`https://gitlab.com/api/v4/projects/${issue.project_id}/issues/${issue.iid}/links`)) {
      if (!link.web_url) continue;

      if (link.web_url.startsWith("https://gitlab.com/gitlab-com/gl-infra/production/")) {
        incidentRelations.push(link.web_url);
      } else if (link.web_url.startsWith("https://gitlab.com/gitlab-com/gl-infra/capacity-planning/")) {
        capacityPlanningRelations.push(link.web_url);
      }
    }

    return {
      incidents: incidentRelations,
      capacityPlanning: capacityPlanningRelations,
      totalLinks: incidentRelations.length + capacityPlanningRelations.length
    };
  }

  getPriority (labels) {
    for (const l of ["priority::1", "priority::2", "priority::3", "priority::4"]) {
      if (labels.has(l)) return l;
    }
  }

  getSeverity (labels) {
    for (const l of ["severity::1", "severity::2", "severity::3", "severity::4"]) {
      if (labels.has(l)) return l;
    }
  }

  async categorizeIssue (issue) {
    const results = new Set();
    const hiddenResults = new Set();
    hiddenResults.add("ALL");

    const labels = new Set(issue.labels);
    const createdAt = parseLocalDate(issue.created_at);

    const firstEvents = await this.getLabelEvents(issue);

    const relatedLinks = await this.categorizeRelatedIssues(issue);

    const priority = this.getPriority(labels);
    const severity = this.getSeverity(labels);

    issue.priority = priority;
    issue.severity = severity;
    issue.relatedLinks = relatedLinks;

    // Priorities
    const prioritySetDate = firstEvents.get(priority);
    const prioritySLADays = PRIORITY_SLA_DAYS[priority];
    if (prioritySetDate && prioritySLADays) {
      hiddenResults.add("HAS_PRIORITY_SLA");

      const prioritySLADate = addDays(prioritySetDate, prioritySLADays);
      issue.prioritySLADate = prioritySLADate;

      if (isBefore(prioritySLADate, Date.now())) {
        results.add("PRIORITY_SLA_VIOLATED");
      }
    }

    // Severities
    const severitySetDate = firstEvents.get(severity);
    const severitySLADays = SEVERITY_SLA_DAYS[severity];
    if (severitySetDate && severitySLADays) {
      hiddenResults.add("HAS_SEVERITY_SLA");

      const severitySLADate = addDays(severitySetDate, severitySLADays);
      issue.severitySLADate = severitySLADate;

      if (isBefore(severitySLADate, Date.now())) {
        results.add("SEVERITY_SLA_VIOLATED");
      }
    }

    if (!issue.milestone) {
      results.add("NO_MILESTONE");

      if (differenceInDays(Date.now(), createdAt) >= 30) {
        results.add("STAGNANT");
      }
    }

    if (issue.milestone && issue.milestone.due_date) {
      const dueDate = parseLocalDate(issue.milestone.due_date);
      issue.milestoneDueDate = dueDate;

      if (isBefore(dueDate, Date.now())) {
        if (labels.has("workflow::verification")) {
          results.add("PAST_DUE_VERIFICATION");
        } else if (labels.has("workflow::in review")) {
          results.add("PAST_DUE_IN_REVIEW");
        } else if (!issue.assignee) {
          results.add("PAST_DUE_UNASSIGNED");
        } else {
          results.add("PAST_DUE");
        }
      }
    }

    if (issue.milestone && issue.milestone.start_date) {
      if (isPastDue(issue.milestone.start_date) && !isPastDue(issue.milestone.due_date)) {
        results.add("CURRENT_MILESTONE");

        if (!issue.assignee) {
          results.add("CURRENT_MILESTONE_BUT_UNASSIGNED");
        }
      }
    }

    if (issue.milestone && issue.milestone.title === "Backlog") {
      results.add("BACKLOG");

      if (differenceInDays(Date.now(), createdAt) >= 90) {
        results.add("STAGNANT");
      }
    }

    const firstLabellingDate = firstEvents.get(this.labelName);
    if (differenceInDays(Date.now(), firstLabellingDate) <= 14) {
      if (relatedLinks.totalLinks === 0) {
        results.add("RECENT_ISSUE_WITHOUT_LINKS");
      } else {
        results.add("RECENT_ISSUE");
      }
    }

    hiddenResults.add("IS_ASSIGNABLE");
    if (issue.assignee) {
      hiddenResults.add("IS_ASSIGNED");
    }

    if (results.size === 0) {
      results.add("OTHER");
    }
    hiddenResults.forEach((f) => results.add(f));

    return results;
  }

  async categorizeIssues () {
    const issueCategories = new Map();
    function add (category, issue) {
      if (issueCategories.has(category)) {
        issueCategories.get(category).push(issue);
      } else {
        issueCategories.set(category, [issue]);
      }
    }

    for await (const issue of urlStreamer(`https://gitlab.com/api/v4/groups/gitlab-org/issues?labels=${this.labelName}&scope=all&state=opened&confidential=true`)) {
      const categories = await this.categorizeIssue(issue);
      issue.categories = categories;
      categories.forEach((category) => add(category, issue));
    }

    for await (const issue of urlStreamer(`https://gitlab.com/api/v4/groups/gitlab-org/issues?labels=${this.labelName}&scope=all&state=opened&confidential=false`)) {
      const categories = await this.categorizeIssue(issue);
      issue.categories = categories;
      categories.forEach((category) => add(category, issue));
    }

    return issueCategories;
  }
}

module.exports = IssueCategorizer;
