"use strict";

const differenceInWeeks = require("date-fns/difference_in_weeks");
const simpleStats = require("simple-statistics");

function ageInWeeks (date) {
  return differenceInWeeks(Date.now(), date);
}

function dueInWeeks (date) {
  return differenceInWeeks(date, Date.now());
}

function max (items, fn) {
  const arr = items.map(fn);
  if (!arr.length) return null;
  return simpleStats.max(arr);
}

function median (items, fn) {
  const arr = items.map(fn).filter((f) => !!f);
  if (!arr.length) return null;
  return simpleStats.median(arr);
}

function display (value, suffix, factor = 1, decimals = 0) {
  if (Number.isNaN(value) || value === null) {
    return "--";
  } else {
    return (value * factor).toFixed(decimals) + " " + suffix;
  }
}

function percentageItems (items, fn) {
  let matchingCount = 0;
  let totalCount = 0;
  items.forEach((item) => {
    const r = fn(item);
    if (r === null) return;
    totalCount++;
    if (r) {
      matchingCount++;
    }
  });

  return matchingCount / totalCount;
}

class InfradevSummarizer {
  constructor (title, items, grouperFilter, percentageCategoryTitle, percentageMatcherFn, slaDateFn) {
    this.title = title;
    this.items = items;
    this.grouperFilter = grouperFilter;
    this.percentageCategoryTitle = percentageCategoryTitle;
    this.percentageMatcherFn = percentageMatcherFn;
    this.slaDateFn = slaDateFn;
  }

  render () {
    const bodyLines = [`## Summary by ${this.title}`];
    bodyLines.push(`|**${this.title}**|**Count**|**Oldest**|**${this.percentageCategoryTitle}**|**Median Age**|**Median Due Time**|% Assigned|`);
    bodyLines.push("|-----------------|---------|----------|-----------------------------------|--------------|-------------------|----------|");

    const groups = this.items.reduce((memo, item) => {
      const key = this.grouperFilter(item);
      if (key !== null) {
        if (memo.has(key)) {
          memo.get(key).push(item);
        } else {
          memo.set(key, [item]);
        }
      }

      return memo;
    }, new Map());

    const keys = Array.from(groups.keys());
    keys.sort();

    keys.forEach((key) => {
      const values = groups.get(key);
      const oldest = max(values, (item) => ageInWeeks(item.created_at));
      const matchingRatio = percentageItems(values, this.percentageMatcherFn);
      const medianAge = median(values, (item) => ageInWeeks(item.created_at));
      const medianSLA = median(values, (item) => {
        const date = this.slaDateFn(item);
        if (date) return dueInWeeks(date);
      });
      const assignedRatio = percentageItems(values, (issue) => (issue.categories.has("IS_ASSIGNABLE") ? issue.categories.has("IS_ASSIGNED") : null));

      const cells = [
        key,
        values.length,
        display(oldest, " weeks"),
        display(matchingRatio, "%", 100, 1),
        display(medianAge, " weeks"),
        display(medianSLA, " weeks"),
        display(assignedRatio, "%", 100, 1)
      ];

      bodyLines.push(`|${cells.join("|")}|`);
    });

    return bodyLines;
  }
}

module.exports = InfradevSummarizer;
