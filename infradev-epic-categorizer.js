"use strict";

const urlStreamer = require("gitlab-issue-report-kit").urlStreamer;
const setHours = require("date-fns/set_hours");
const setMinutes = require("date-fns/set_minutes");
const setSeconds = require("date-fns/set_seconds");
const setMilliseconds = require("date-fns/set_milliseconds");
const isBefore = require("date-fns/is_before");
const differenceInDays = require("date-fns/difference_in_days");
const addDays = require("date-fns/add_days");
const parse = require("date-fns/parse");

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority
const PRIORITY_SLA_DAYS = {
  "priority::1": 30,
  "priority::2": 90
};

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
const SEVERITY_SLA_DAYS = {
  "severity::1": 30,
  "severity::2": 60,
  "severity::3": 90
};

function parseLocalDate (date) {
  let d = parse(date);
  d = setHours(d, 0);
  d = setMinutes(d, 0);
  d = setSeconds(d, 0);
  d = setMilliseconds(d, 0);
  return d;
}

class InfradevEpicCategorizer {
  async getLabelEvents (epic) {
    const firstEvents = new Map();

    try {
      for await (const event of urlStreamer(`https://gitlab.com/api/v4/groups/${epic.group_id}/epics/${epic.iid}/resource_label_events`)) {
        if (event.action !== "add" || !event.label) {
          continue;
        }

        const createdAt = parse(event.created_at);
        const label = event.label.name;
        if (!firstEvents.has(label)) {
          firstEvents.set(label, createdAt);
        }
      }
    } catch (e) {} // eslint-disable-line no-empty
    return firstEvents;
  }

  getPriority (labels) {
    for (const l of ["priority::1", "priority::2", "priority::3", "priority::4"]) {
      if (labels.has(l)) return l;
    }
  }

  getSeverity (labels) {
    for (const l of ["severity::1", "severity::2", "severity::3", "severity::4"]) {
      if (labels.has(l)) return l;
    }
  }

  isSLAViolation (startDate, slaDays) {
    if (!startDate || !slaDays) return false;
    const slaDate = addDays(startDate, slaDays);
    return isBefore(slaDate, Date.now());
  }

  async categorizeEpic (epic) {
    const results = new Set();
    const hiddenResults = new Set();
    hiddenResults.add("ALL");

    const labels = new Set(epic.labels);
    const dueDate = parseLocalDate(epic.due_date);
    const createdAt = parse(epic.created_at);

    const firstEvents = await this.getLabelEvents(epic);

    // let relatedLinks = await this.categorizeRelatedIssues(epic);

    const priority = this.getPriority(labels);
    const severity = this.getSeverity(labels);

    epic.priority = priority;
    epic.severity = severity;
    epic.relatedLinks = { incidents: [], capacityPlanning: [], totalLinks: 0 };

    const prioritySetDate = firstEvents.get(priority) || createdAt;
    const severitySetDate = firstEvents.get(severity) || createdAt;

    const prioritySLADays = PRIORITY_SLA_DAYS[priority];
    const severitySLADays = SEVERITY_SLA_DAYS[severity];

    const priorityViolation = this.isSLAViolation(prioritySetDate, prioritySLADays);
    if (priorityViolation) {
      results.add("PRIORITY_SLA_VIOLATED");
    }

    const severityViolation = this.isSLAViolation(severitySetDate, severitySLADays);
    if (severityViolation) {
      results.add("SEVERITY_SLA_VIOLATED");
    }

    if (isBefore(dueDate, Date.now())) {
      results.add("EPIC_PAST_DUE");
    }

    const firstInfradevLabellingDate = firstEvents.get("infradev");
    if (differenceInDays(Date.now(), firstInfradevLabellingDate) <= 14) {
      results.add("RECENT_EPIC");
    }

    if (results.size === 0) {
      results.add("OTHER_EPIC");
    }

    hiddenResults.forEach(f => results.add(f));

    return results;
  }

  async categorizeInfradevEpics () {
    const epicCategories = new Map();
    function add (category, issue) {
      if (epicCategories.has(category)) {
        epicCategories.get(category).push(issue);
      } else {
        epicCategories.set(category, [issue]);
      }
    }

    for await (const epic of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-org/epics?labels=infradev&scope=all&state=opened&confidential=true")) {
      const categories = await this.categorizeEpic(epic);
      epic.categories = categories;
      categories.forEach((category) => add(category, epic));
    }

    for await (const epic of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-org/epics?labels=infradev&scope=all&state=opened&confidential=false")) {
      const categories = await this.categorizeEpic(epic);
      epic.categories = categories;
      categories.forEach((category) => add(category, epic));
    }

    return epicCategories;
  }
}

module.exports = InfradevEpicCategorizer;
