const fetchStageGroups = require("./stage-groups");
const fetchErrorBudgets = require("./error-budgets");
const _ = require("lodash");
const httpClient = require("gitlab-issue-report-kit").httpClient;
const startOfMonth = require("date-fns/start_of_month");
const getTime = require("date-fns/get_time");

const PRODUCTION_PROJECT = "gitlab-com/gl-infra/production";
const PRODUCTION_PROJECT_FULL_URL = `https://gitlab.com/${PRODUCTION_PROJECT}`;
const issueUrlRegex = new RegExp(`^${_.escapeRegExp(PRODUCTION_PROJECT_FULL_URL + "/-/issues/")}(.*)`);

function labelsMatch (a, b) {
  return a.toLowerCase().replace(/ /g, "_") === b.toLowerCase().replace(/ /g, "_");
}

function display (value, suffix = "", factor = 1, decimals = 0) {
  if (Number.isNaN(value) || value === null || value === undefined) {
    return "--";
  } else {
    return (value * factor).toFixed(decimals) + suffix;
  }
}

function getSeverity (issue) {
  for (const l of issue.labels) {
    if (l.startsWith("severity::")) {
      const [, severityText] = l.split("::");
      return parseInt(severityText, 10);
    }
  }
}

function getSevEmoji (sev) {
  switch (sev) {
    case 1:
      return ":one:";
    case 2:
      return ":two:";
    case 3:
      return ":three:";
    case 4:
      return ":four:";
    default:
      return ":hash:";
  }
}

function mdLabelize(prefix, value) {
  const labelValue = [prefix, value]
        .map(function(v) { return v.replace(/_/g, " ") })
        .join("::")

  return `~"${labelValue}"`
}

class EngineeringAllocator {
  async fetch (allInfradevItems) {
    this.stageGroups = await fetchStageGroups();
    this.errorBudgets = await fetchErrorBudgets();
    this.incidents = await this.fetchIncidents(allInfradevItems);
  }

  // Walks through all infradev issues,
  // and fetches the related incidents
  // for further analysis
  async fetchIncidents (allInfradevItems) {
    const allIncidentIssues = allInfradevItems
      .flatMap(function (issue) {
        return issue?.relatedLinks?.incidents;
      })
      .flatMap((url) => {
        const m = issueUrlRegex.exec(url);
        if (m) {
          return [m[1]];
        } else {
          return [];
        }
      });

    const incidentMap = new Map();
    this.incidentMap = incidentMap;

    const uniqueIncidents = _.uniq(allIncidentIssues);
    for (const issueId of uniqueIncidents) {
      const [result] = await httpClient(`https://gitlab.com/api/v4/projects/${encodeURIComponent(PRODUCTION_PROJECT)}/issues/${issueId}`);
      incidentMap.set(result.web_url, result);
    }
  }

  async report (allInfradevItems, errorBudgetItems) {
    await this.fetch(allInfradevItems);

    const report = this.stageGroups.map((stageGroup) => {
      const label = `group::${stageGroup.id.replace(/_/g, " ")}`;
      const budget = this.errorBudgets[stageGroup.id];
      return {
        id: stageGroup.id,
        label,
        groupLabel: mdLabelize("group", stageGroup.id),
        errorBudget: budget?.budget,
        weekBudget: budget?.weekBudget,
        trafficShare: budget?.share,
        trafficShareHasSignificance: budget?.share >= 0.0001,
        apdexComponent: budget?.apdexComponent,
        errorComponent: budget?.errorComponent,
        section: stageGroup.section,
        sectionLabel: mdLabelize("section", stageGroup.section),
        stage: stageGroup.stage,
        infradevIssueCount: this.countIssues(allInfradevItems, stageGroup.id),
        errorBudgetIssueCount: this.countIssues(errorBudgetItems, stageGroup.id),
        incidents: this.collectIncidents(allInfradevItems, stageGroup.id)
      };
    });

    let filtered = report.filter((item) => {
      return item.infradevIssueCount || item.incidentCount || item.errorBudget;
    });

    filtered = _.sortBy(filtered, [
      // Sort by number of issues, desc
      (o) => -o.incidents.length,
      // Sort by error rate * traffic share
      (o) => {
        const share = o.trafficShare || 0;
        const weekBudget = o.weekBudget || 1;
        const error = 1 - weekBudget;
        const sortScore = share * error;
        return -sortScore;
      },
      (o) => o.section,
      (o) => o.groupLabel
    ]);

    function errorBudgetColumn (item, key, range) {
      if (!item.trafficShareHasSignificance) {
        // Don't display error budget values for stage groups with less than 0.01% of traffic
        return "";
      }

      const value = display(item[key], "%", 100, 2);
      const link = `[${value}](https://dashboards.gitlab.net/d/stage-groups-detail-${item.id}?from=now-${range})`;

      if (item[key] && item[key] < 0.9995) {
        return `**${link}** ❌`;
      }

      if (item[key]) {
        return `${link} 🟢`;
      }

      return link;
    }

    function apdexComponentColumn (item) {
      if (!item.trafficShareHasSignificance) {
        // Don't display error budget values for stage groups with less than 0.01% of traffic
        return "";
      }

      return display(item.apdexComponent, "%", 100, 2);
    }

    function errorComponentColumn (item) {
      if (!item.trafficShareHasSignificance) {
        // Don't display error budget values for stage groups with less than 0.01% of traffic
        return "";
      }

      return display(item.errorComponent, "%", 100, 2);
    }

    function errorBudgetIssuesColumn (item) {
      let value = display(item.errorBudgetIssueCount);
      if (item.errorBudgetIssueCount && item.errorBudgetIssueCount > 0) {
        value = `**${value}**`;
      }

      return `[${value}](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=Error+Budget+Improvement&label_name[]=${encodeURIComponent(
        item.label
      )})`;
    }

    function infradevColumn (item) {
      let value = display(item.infradevIssueCount);
      if (item.infradevIssueCount && item.infradevIssueCount > 0) {
        value = `**${value}**`;
      }

      const handbookReportPermalink = item.id.replace(/ _/g, "-");
      const handbookReport = `https://about.gitlab.com/handbook/engineering/metrics/${item.section}/${item.stage}/#${handbookReportPermalink}-group-infrastructure-dashboard`;

      return `[${value}](http://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=${encodeURIComponent(
        item.label
      )}) <small>[📊](${handbookReport})</small>`;
    }

    function incidentColumn (item) {
      const incidentCount = item.incidents.length;
      let value = display(incidentCount);
      if (incidentCount && incidentCount > 0) {
        value = `**${value}**`;
      }
      return value;
    }

    function incidentLinksColumn (item) {
      let month = null;

      const incidentMarkdowns = item.incidents.map((i) => {
        const m = getTime(startOfMonth(i.created_at));
        let monthSep = "";

        if (month && month !== m) {
          // Add a separator when month of incident changes
          monthSep = " \\| ";
        }
        month = m;

        const sev = getSeverity(i);
        return `${monthSep}[${getSevEmoji(sev)}](${i.web_url})`;
      });

      return incidentMarkdowns.join(" ");
    }

    function trafficShareColumn (item) {
      let value = display(item.trafficShare, "%", 100, 3);
      if (item.trafficShareHasSignificance) {
        value = `**${value}**`;
      }

      return value;
    }

    return [
      "| **Section** | **Group** | **Traffic Share** (past 28d)|**Error Budget** (past 28d)| **Error Budget** (past 7d) | **Apdex Component** (past 28d)| **Error Component**  (past 28d)| **Error Budget issues** | **Infradev Issues** | **Related Incidents via Infradev Issues** | **Incidents** |",
      "|---|---|---|---|---|---|---|---|---|---|---|"
    ].concat(
      filtered.flatMap((item) => {
        const cells = [
          item.sectionLabel,
          item.groupLabel,
          trafficShareColumn(item),
          errorBudgetColumn(item, "errorBudget", "28d"),
          errorBudgetColumn(item, "weekBudget", "7d"),
          apdexComponentColumn(item),
          errorComponentColumn(item),
          errorBudgetIssuesColumn(item),
          infradevColumn(item),
          incidentColumn(item),
          incidentLinksColumn(item)
        ];

        return `|${cells.join("|")}|`;
      })
    );
  }

  collectIncidents (issues, stageGroup) {
    const stageGroupLabel = `group::${stageGroup}`;
    const allIssues = issues.flatMap(function (issue) {
      const i = issue.labels.findIndex((item) => labelsMatch(item, stageGroupLabel));
      if (i >= 0) {
        return issue?.relatedLinks?.incidents;
      } else {
        return [];
      }
    });

    const uniqueIssuesUrls = _.uniq(allIssues);

    const incidentIssues = uniqueIssuesUrls.flatMap((webUrl) => {
      const issue = this.incidentMap.get(webUrl);
      if (issue && issue.labels.includes("incident")) {
        return [issue];
      }

      return [];
    });

    return _.sortBy(incidentIssues, (i) => i.created_at);
  }

  countIssues (issues, stageGroup) {
    const stageGroupLabel = `group::${stageGroup}`;
    return issues.reduce(function (count, issue) {
      const i = issue.labels.findIndex((item) => labelsMatch(item, stageGroupLabel));
      if (i >= 0) {
        return count + 1;
      }
      return count;
    }, 0);
  }
}

module.exports = EngineeringAllocator;
